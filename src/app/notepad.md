managed - directed
responsible for - oversaw
worked/organised - orchestrated
did - accomplished
helped - facilitated
made - crafted
handled - controlled
was part of - participated
contributed to - collaborated on
gained experience - acquired proficiency
managed - supervised
ran - executed
conducted - carried out
utilised - leveraged
wrote - authored

feat: Added form for editing cover letter easily

- Added CoverLetterForm modal to the coverletter page
- Removed the typo in the CV phone number

branch: feat/coverletter-editor
