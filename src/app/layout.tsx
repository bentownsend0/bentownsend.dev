'use client'

import { fonts } from '../fonts'
import { Providers } from './providers'
import { theme } from './theme'

//import { cookies as nextCookies } from 'next/headers'
import { Box, ColorModeScript, HStack, useColorModeValue } from '@chakra-ui/react'

import { Header } from './components/organisms/Header'
import { Footer } from './components/organisms/Footer'
import { ContactModal } from './components/organisms/ContactModal'
import { Link } from '@chakra-ui/next-js'

export default function RootLayout({
  children
}: {
  children: React.ReactNode
}) {
  //const appCookies = nextCookies()

  return (
    <html lang='en' className={`${fonts.heading.variable} ${fonts.text.variable}`}>
      <head>
        <title>Ben Townsend - Leadership, DevOps &amp; Full-Stack Development</title>

        <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="author" content='Ben Townsend' />
        <meta name="description" content='Ben Townsend: Experienced DevOps Engineer specialising in scalable infrastructure, full-stack development, and team leadership. Discover my portfolio of past projects and learn how I can help drive your tech solutions forward.' />
        <meta name="keywords" content="DevOps, Full-Stack Development, AWS, Scalable Infrastructure, CI/CD, Cloud Computing"></meta>
        <meta name='viewport' content='width=device-width, initial-scale=1.0' />
      </head>
      <Box as='body' sx={{ '@media print': { fontSize: '75%' } }}>
        <Providers>
          <ColorModeScript initialColorMode={theme.config.initialColorMode} />
          <Header>
            <HStack>
              <Link href='/cv' px='3' py='2'>CV</Link>
            </HStack>
            <ContactModal disabled></ContactModal>
          </Header>
          <Box as='main'>
            {children}
          </Box>
          <Footer></Footer>
        </Providers>
      </Box>
    </html >
  )
}
