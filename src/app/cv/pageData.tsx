import { ReactNode } from 'react'
import { Icon, IconProps } from '@chakra-ui/react'
import { FaLinkedin as LinkedinIcon, FaLink as LinkIcon, FaLocationArrow as MapIcon, FaPhone as PhoneIcon, FaRegEnvelope as EmailIcon } from 'react-icons/fa'

import { RelayIcon } from '../components/atoms/RelayIcon'

interface RoleProps {
  date: string
  position: string
  tasks?: Array<string>
}

interface JobProps {
  company: string
  roles: Array<RoleProps>
}

interface ContactProps {
  href: string
  icon?: ReactNode
  text: string
}

export const certs: Array<String> = [
  'AWS Solutions Architect Associate'
]

export const contacts: Array<ContactProps> = [{
  href: `tel:${process.env.NEXT_PUBLIC_CONTACT_NUMBER}`,
  icon: <Icon as={PhoneIcon} h='.75em' mr='1' />,
  text: process.env.NEXT_PUBLIC_CONTACT_NUMBER || "Phone"
}, {
  href: `mailto:${process.env.NEXT_PUBLIC_CONTACT_PR_EMAIL}`,
  icon: <Icon as={EmailIcon} h='1em' mr='1' verticalAlign='middle' />,
  text: process.env.NEXT_PUBLIC_CONTACT_PR_EMAIL || "Email"
}, {
  href: `https://${process.env.NEXT_PUBLIC_CONTACT_LINKEDIN}`,
  icon: <Icon as={LinkedinIcon} h='1em' mr='1' verticalAlign='middle' />,
  text: process.env.NEXT_PUBLIC_CONTACT_LINKEDIN || 'Linkedin'
}, {
  href: `https://${process.env.NEXT_PUBLIC_CONTACT_WEBSITE}`,
  icon: <Icon as={LinkIcon} h='.75em' mr='1' />,
  text: process.env.NEXT_PUBLIC_CONTACT_WEBSITE || 'Website'
}, {
  href: `${process.env.NEXT_PUBLIC_CONTACT_LOCATION}`,
  icon: <Icon as={MapIcon} h='.75em' mr='1' />,
  text: 'Redhill, UK'
}]

export const skills: Array<string> = [
  'Delivering business goals through the intelligent use of digital technologies.',
  'Collaborating with stakeholders to balance changing prioritise.',
  'Team leadership.',
  'Fostering a positive team environment by understanding & supporting colleagues\' needs & challenges.',
  'Expert in distilling complex technical concepts into clear, actionable insights for stakeholders at all levels, ensuring alignment & driving informed decision-making.',
  'An ability to thrive in fast-paced, dynamic environments, particularly in startup settings, where adaptability & quick decision-making are crucial.'
]

// export const summary: string = 'I am an accomplished software engineer with over a decade of experience in full-stack development and DevOps. Throughout my career, I have consistently driven innovation by creating scalable and secure infrastructure solutions that significantly enhance business performance. I am recognised for my leadership in mentoring teams, optimising processes, and successfully delivering projects that not only meet, but exceed expectations. My ability to translate complex technical challenges into actionable strategies has resulted in measurable impacts on both revenue and customer satisfaction, aligning technology with business goals effectively.'
export const summary: string = 'Accomplished engineer with 10+ years in DevOps and full-stack development. I drive innovation through scalable, secure infrastructure solutions that enhance business performance. Known for mentoring teams, optimizing processes, and delivering projects that exceed expectations. I effectively translate challenges into strategies that align technology with business goals.'

export const tech: Array<string> = [
  'AWS',
  'CSS',
  'DataDog',
  'Docker',
  'Fastly',
  'Go',
  'Grafana',
  'HTML',
  'Javascript',
  'Jenkins',
  'Kubernetes',
  'Linux',
  'Markdown',
  'MySQL',
  'Netlify',
  'New Relic',
  'Next.js',
  'NGINX',
  'Puppet',
  'React',
  'Ruby templating',
  'SCSS',
  'Shell scripting',
  'Terraform',
  'Typescript',
  'Vagrant',
  'YAML'
]

export const jobs: Array<JobProps> = [{
  company: 'Immediate Media',
  roles: [{
    date: 'Apr 2023 - Jan 2024',
    position: 'DevOps Engineer',
    tasks: [
      'Implementing a service mesh system to improve service response times & allow the new native app platform to use backend APIs.',
      'Improving Docker image tagging to enable image retention cycles, greatly reducing storage costs.',
      'Leading the conversation on which solutions should be investigated when looking to reduce the costs of application monitoring.',
      'Managing information & system access using detailed IAM & Security Group rules.',
    ]
  }]
}, {
  company: 'New Scientist',
  roles: [{
    date: 'Dec 2020 - Aug 2022',
    position: 'DevOps Engineer',
    tasks: [
      'Day-to-day running of all things DevOps as the principal DevOps Engineer.',
      'Redesigned application networking with a focus on the least privilege principal, enhancing security.',
      'Rebuilt the Jenkins server using Docker & ECS, to improve security & streamline the upgrade process.',
      'Merging the OS specific local developer environments to one using Docker, ready for the introduction of Apple Silicon & to improve developer onboarding.',
      'Facilitating AWS account migration after acquisition by the Daily Mail General Trust.',
      'Refactoring the Fastly VCL to allow for authentication at the edge & faster request routing.',
    ]
  }]
}, {
  company: 'Immediate Media',
  roles: [{
    date: 'May 2018 - Dec 2020',
    position: 'DevOps Engineer',
    tasks: [
      'Architecting the infrastructure for a new redirect handler, improving response times leading to better SEO & bringing containerisation & "serverless" functions to the business.',
      'Introducing AWS Transit Gateway to the business for faster, cheaper & more secure traffic handling between different systems.',
      'Spearheading the rebuild of the deployment pipeline for V1 of the Digital publishing platform.',
      'Running data migration & managing brand website relaunches without downtime.',
    ]
  }, {
    date: 'Oct 2017 - May 2018',
    position: 'PHP Developer',
    tasks: [
      'Data processing & migration for RadioTimes.',
      'Building the venues feature used by Gardeners World & future migrated brands.',
      'Database query optimisation to improve server response times.',
    ]
  }, {
    date: 'Jul 2016 - Sep 2017',
    position: 'WordPress Developer',
    tasks: [
      'Initial fullstack development for the new publishing platform.',
      'Migration of Olive Magazine to the new platform.',
    ]
  }]
}, {
  company: 'Freelance',
  roles: [{
    date: 'Dec 2015 - Jun 2016',
    position: 'Fullstack Developer',
  }]
}, {
  company: 'Beef Digital',
  roles: [{
    date: 'Sep 2013 - Dec 2015',
    position: 'Fullstack Developer'
  }]
}]

export const interests: Array<string> = [
  'Design of all types: Architecture, Interiors, Web, Print, etc.',
  'Consumer & smart home technology.',
  'Travel; I enjoy different cultures, climates & local cuisine.',
  'Food.',
  'Music.'
]