'use client'

import { Box, Container, Heading, Icon, IconButton, List, ListIcon, ListItem, Spacer, Stack, Text, UnorderedList, useColorModeValue } from '@chakra-ui/react'
import { Link } from '@chakra-ui/next-js'
import { FaCertificate as CertIcon, FaDownload as DownloadIcon, FaTools as RoleIcon, FaPrint as PrintIcon, FaDiaspora as ReadyIcon } from 'react-icons/fa'

import { certs, contacts, interests, jobs, skills, summary, tech } from './pageData'

export default function Page() {
  const bgColour = useColorModeValue('white', 'gray.700')
  const titleBlue = useColorModeValue('blue.900', 'blue.200')
  const titlePink = useColorModeValue('pink.700', 'pink.300')

  return (
    <Box as='main' backgroundColor={bgColour} pb='3' sx={{ '@media print': { backgroundColor: 'white', color: 'gray.900', m: 0, zoom: '.75' } }}>
      <Container maxWidth='5xl' px='8' pb='10' pos='relative' sx={{ '@media print': { p: 0, m: 0 } }}>
        <Stack align='center' direction='row' justify='space-between' sx={{ '@media not print': { display: 'none' } }}>
          <Heading as='h1' color='blue.900' size='2xl' pb='1'>Ben Townsend</Heading>
          <Heading as='h4' color='pink.700' fontWeight={400} size='md'><Icon as={ReadyIcon} h='.75em' mr='1' />Available Immediately</Heading>
        </Stack>

        <Stack color={titlePink} direction={{ base: 'column', md: 'row' }} justify='space-between' pb='1' pt='10' sx={{ '@media print': { color: 'pink.700', flexDirection: 'row', pt: '0' } }} wrap='wrap'>
          {contacts.map((contact, index) =>
            <Link href={contact.href} isExternal key={`contact${index}`}>
              <Heading as='h4' fontWeight={400} size='md'>{contact.icon && contact.icon}{contact.text}</Heading>
            </Link>
          )}
        </Stack>

        <Box pb='5'>
          <Text>{summary}</Text>
        </Box>

        <Box mb='4' p='3' border='1px solid' rounded='md'>
          <Stack align='top' direction={{ base: 'column', sm: 'row' }} spacing='5' sx={{ '@media print': { flexDirection: 'row' } }}>
            {/* <Box w={{ base: '100%', sm: '52%' }} pr={{ base: '0', sm: '2%' }} sx={{ '@media print': { w: '52%', pr: '2%' } }}>
              <Heading as='h2' color={titleBlue} size='xl' pb='3' sx={{ '@media print': { color: 'blue.900' } }}>Skills</Heading>
              <UnorderedList styleType="'-  '">
                {skills.map((skill, index) => <ListItem key={index}>{skill}</ListItem>)}
              </UnorderedList>
            </Box> */}
            {/* <Box w={{ base: '100%', sm: '52%' }} sx={{ '@media print': { w: '52%' } }}> */}
            <Box>
              <Heading as='h2' color={titleBlue} size='lg' sx={{ '@media print': { color: 'blue.900' } }}>Technologies</Heading>
              <Stack direction='row' gap={1.5} wrap='wrap'>
                {tech.map((item, index) =>
                  <Text key={`tech${index}`}>{item}</Text>
                )}
              </Stack>
            </Box>
            <Box pb='3'>
              <Heading as='h2' color={titleBlue} size='lg' pb='3' sx={{ '@media print': { color: 'blue.900' } }}>Certification</Heading>
              <List whiteSpace='nowrap'>
                {certs.map((cert, index) =>
                  <ListItem key={`cert${index}`} pb='1' pl='1em'>
                    <ListIcon as={CertIcon} ml='-1em' verticalAlign='inherit' />
                    {cert}
                  </ListItem>)}
              </List>
            </Box>
            {/* </Box> */}
          </Stack>
        </Box>

        <Box>
          {/* <Heading as='h2' color={titleBlue} size='xl' pb='1' sx={{ '@media print': { color: 'blue.900' } }}>Employment</Heading> */}
          <Stack direction='row' gap='0' wrap='wrap'>
            {jobs.map((employer, index) =>
              <Box key={`employer${index}`} gap='0' minW='50%' w={employer.roles[index]?.tasks ? '100%' : 'auto'}>
                <Heading as='h3' color={titlePink} size='lg' pt='1' sx={{ '@media print': { color: 'pink.700' } }}>{employer.company}</Heading>

                {employer.roles.map((role, index) =>
                  <Box key={`role${index}`}>

                    <Stack direction='row' align='baseline' borderBottom={role.tasks ? '1px' : '0'} gap='1' w='100%'>
                      <Heading as='h4' color={titleBlue} fontWeight={400} size='md' sx={{ '@media print': { color: 'blue.900' } }}>{role.position}</Heading>
                      <Heading as='h4' fontWeight={400} size='md'>&bull; {role.date}</Heading>
                    </Stack>
                    {role.tasks &&
                      <Box pt='3'>
                        <Heading as='h5' fontWeight={400} size='sm'>Notable work:</Heading>
                        <UnorderedList pb='3' pt='1' styleType="'-  '">
                          {role.tasks.map((task, index) =>
                            <ListItem key={`task${index}`}>{task}</ListItem>
                          )}
                        </UnorderedList>
                      </Box>
                    }

                  </Box>
                )}

              </Box>
            )}
          </Stack>
        </Box>

        {/* <Box pb='10' pt='3'>
          <Stack align='top' direction={{ base: 'column-reverse', sm: 'row' }} sx={{ '@media print': { flexDirection: 'row' } }}>
            <Box w={{ base: '100%', sm: '52%' }} pr={{ base: '0', sm: '2%' }} sx={{ '@media print': { w: '52%', pr: '2%' } }}>
              <Heading as='h2' color={titleBlue} size='xl' pb='3' sx={{ '@media print': { color: 'blue.900' } }}>Education</Heading>
              <UnorderedList pb='3' styleType="'-  '">
                <ListItem key='education1'>Hertford Regional College &bull; BTEC Music Technology &bull; Distinction</ListItem>
                <ListItem key='education1'>Hockerill Anglo European College &bull; GSCEs &bull; As & Bs</ListItem>
              </UnorderedList>
              <Heading as='h2' color={titleBlue} size='xl' pb='3' sx={{ '@media print': { color: 'blue.900' } }}>Languages</Heading>
              <UnorderedList pb='3' styleType="'-  '">
                <ListItem key='education1'>English &bull; Native</ListItem>
                <ListItem key='education2'>German &bull; GCSE</ListItem>
                <ListItem key='education3'>Japanese &bull; Beginner</ListItem>
              </UnorderedList>
            </Box>
            <Box w={{ base: '100%', sm: '48%' }} sx={{ '@media print': { w: '48%' } }}>
              <Heading as='h2' color={titleBlue} size='xl' pb='3' sx={{ '@media print': { color: 'blue.900' } }}>Interests</Heading>
              <UnorderedList pb='3' styleType="'-  '">
                {interests.map((interest, index) => <ListItem key={`interest${index}`}>{interest}</ListItem>)}
              </UnorderedList>
            </Box>
          </Stack>
        </Box> */}

        <Stack bottom='3' direction='row' gap='3' pos='fixed' right='8' sx={{ '@media print': { display: 'none' } }}>
          <Link href='/BenTownsendCV.pdf' download='BenTownsendCV.pdf' isExternal>
            <IconButton
              aria-label='Download CV'
              colorScheme='blue'
              icon={<DownloadIcon />}
              size='md'
            />
          </Link>
          <IconButton
            aria-label='Print CV'
            colorScheme='blue'
            icon={<PrintIcon />}
            onClick={() => window.print()}
            size='md'
          />
        </Stack>
      </Container>
    </Box >
  )
}