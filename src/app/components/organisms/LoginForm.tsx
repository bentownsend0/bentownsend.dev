import {
  Button,
  FormControl,
  FormErrorMessage,
  FormHelperText,
  FormLabel,
  Modal,
  ModalHeader,
  ModalBody,
  ModalContent,
  ModalCloseButton,
  ModalFooter,
  ModalOverlay,
  useDisclosure,
  IconButton
} from '@chakra-ui/react'
import { Formik, FormikHelpers, FormikProps } from 'formik'
import { FaEnvelope as MailIcon } from 'react-icons/fa';
import { Input } from '../molecules/Input'

interface FormValues {
  fullname?: string
  email?: string;
  message?: string;
}

interface ModalProps {
  disabled?: boolean
}

export function LoginForm({ disabled }: ModalProps) {

  const { isOpen, onOpen, onClose } = useDisclosure()
  const initialValues: FormValues = { fullname: undefined, email: undefined, message: undefined }

  const handleFormSubmit = (values: FormValues, actions: FormikHelpers<FormValues>) => {
    //event.preventDefault();
    //const formData = new FormData(event.target);
    console.log({ values, actions });
    alert(JSON.stringify(values, null, 2));
    actions.setSubmitting(false);
  }

  return (
    <>
      <IconButton aria-label='Contact Modal' icon={<MailIcon />} colorScheme='blue' isDisabled={disabled} onClick={onOpen} />

      <Modal isOpen={isOpen} onClose={onClose} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Contact Me</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Formik initialValues={initialValues} onSubmit={handleFormSubmit} >
              {(props: FormikProps<FormValues>) => (
                <form onSubmit={props.handleSubmit} data-netlify-recaptcha='true' data-netlify='true'>
                  <FormControl variant='floating' id='name' isRequired pb={3}>
                    {/* <Input name='fullname' /> */}
                    <FormLabel>First name</FormLabel>
                    <FormHelperText>How should I address you?</FormHelperText>
                    <FormErrorMessage>Your First name is invalid!</FormErrorMessage>
                  </FormControl>
                  <FormControl variant='floating' id='name' isRequired pb={3}>
                    {/* <Input name='email' /> */}
                    <FormLabel>Email Address</FormLabel>
                    <FormHelperText>How do I contact you?</FormHelperText>
                    <FormErrorMessage>Your Email Address is invalid!</FormErrorMessage>
                  </FormControl>
                  <FormControl variant='floating' id='name' isRequired>
                    {/* <Input name='message' /> */}
                    <FormLabel>Message</FormLabel>
                    <FormHelperText>Just a quick note.</FormHelperText>
                    <FormErrorMessage>Your Message is invalid!</FormErrorMessage>
                  </FormControl>
                  <div data-netlify-recaptcha="true"></div>
                </form>
              )}
            </Formik>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme='blue' mr={3} type='submit'>Send</Button>
            <Button onClick={onClose} variant='ghost'>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal >
    </>
  )
}