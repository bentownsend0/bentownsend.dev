import { Box, Heading, HStack, IconButton, LinkProps, Stack, Spacer, Text, useColorModeValue } from '@chakra-ui/react'
import { Link } from '@chakra-ui/next-js'
import { FaLinkedin, FaInstagram } from 'react-icons/fa'

const links: Array<String> = [];
const accounts = [
  {
    url: 'https://linkedin.com/in/bentownsend0',
    label: 'LinkedIn Account',
    type: 'blue',
    icon: <FaLinkedin />
  },
  {
    url: 'https://www.instagram.com/fourwheelsfourfeet/',
    label: 'Instagram Account',
    type: 'pink',
    icon: <FaInstagram />
  }
];

export function Footer({ ...props }) {
  const textColour = useColorModeValue('gray.900', 'gray.100')
  const currentYear = new Date().getFullYear()

  return (
    <Stack
      as="footer"
      marginInline="auto"
      p={8}
      spacing={{ base: 1, md: 'auto' }}
      justifyContent="space-between"
      alignItems="center"
      direction={{ base: 'column', md: 'row' }}
      sx={{ '@media print': { display: 'none' } }}
      {...props}
    >
      <Box color={textColour} textAlign={{ base: 'center', md: 'left' }}>
        <Heading size='md' mb='3'>Ben Townsend</Heading>
        <Text as='p'>&copy; 2019 - {currentYear}</Text>
      </Box>

      <Spacer />

      {/* Desktop Screen */}
      <HStack spacing={4} alignItems="center" display={{ base: 'none', md: 'flex' }}>
        {links?.map((link, index) => (
          <CustomLink key={index}>{link}</CustomLink>
        ))}
      </HStack>

      <Spacer />

      {/* Mobile and Tablet Screens */}
      {/* <Stack display={{ base: 'flex', md: 'none' }} alignItems="center">
        <HStack alignItems="center">
          <CustomLink>Sign up</CustomLink>
          <Divider h="1rem" orientation="vertical" />
          <CustomLink>Blog</CustomLink>
          <Divider h="1rem" orientation="vertical" />
          <CustomLink>Career</CustomLink>
        </HStack>
        <HStack alignItems="center">
          <CustomLink>Documentation</CustomLink>
          <Divider h="1rem" orientation="vertical" />
          <CustomLink>Terms of use</CustomLink>
        </HStack>
        <CustomLink>Privacy policy</CustomLink>
      </Stack> */}

      <Stack direction="row" spacing={5} alignItems="center">
        {accounts.map((sc, index) => (
          <IconButton
            key={index}
            as={Link}
            isExternal
            href={sc.url}
            aria-label={sc.label}
            colorScheme={sc.type}
            icon={sc.icon}
            rounded="md"
          />
        ))}
      </Stack>
    </Stack>
  );
}

export function CustomLink({ children, ...props }: LinkProps) {
  return (
    <Link href="#" fontSize="sm" _hover={{ textDecoration: 'underline' }} {...props}>
      {children}
    </Link>
  );
};
