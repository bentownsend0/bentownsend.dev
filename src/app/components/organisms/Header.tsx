import { ReactNode } from 'react'
import { Box, ButtonGroup, ChakraProps, Flex, Heading, IconButton, Modal, ModalContent, Show, Spacer, useColorModeValue, useDisclosure } from '@chakra-ui/react'
import { Link } from '@chakra-ui/next-js'
import { FaBars as MenuIcon } from 'react-icons/fa'
import { ThemeChangeButton } from '../molecules/ThemeChangeButton'

interface Props {
  children?: ReactNode
  props?: ChakraProps
}

export function Header({ children, ...props }: Props) {

  const bgColour = useColorModeValue('white', 'gray.900')
  const borderColour = useColorModeValue('gray.100', 'transparent')
  const titleColour = useColorModeValue('gray.900', 'gray.100')
  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <Flex as='header' minWidth='max-content' alignItems='center' gap='2' bg={bgColour} borderBottom='1px solid' borderBottomColor={borderColour} sx={{ '@media print': { display: 'none' } }} {...props}>
      <Box px={{ base: '3', md: '8' }} py='2'>
        <Heading as={Link} href='/' size='lg' color={titleColour} mb='-4px'>Ben Townsend</Heading>
      </Box>

      <Spacer />
      <ButtonGroup as='nav' gap='1' mr={{ base: '3', md: '8' }} my='2'>
        {children}
        <ThemeChangeButton></ThemeChangeButton>
      </ButtonGroup>
      {/* <Show below='sm'>
        <ButtonGroup as='nav' gap='2' mr='3' my='2'>
          <IconButton aria-label='Mobile menu button' icon={<MenuIcon />} onClick={onOpen} size='md' />
          <ThemeChangeButton></ThemeChangeButton>
        </ButtonGroup>
        <Modal isOpen={isOpen} onClose={onClose}>
          <ModalContent px='5' py='8'>
            {children}
          </ModalContent>
        </Modal>
      </Show> */}
    </Flex >
  )
}
