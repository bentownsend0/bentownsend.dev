import { IconButton, useColorMode } from '@chakra-ui/react'
//import { MoonIcon, SunIcon } from '@chakra-ui/icons'
import { FaMoon as MoonIcon, FaSun as SunIcon } from 'react-icons/fa'

export function ThemeChangeButton() {
  const { colorMode, toggleColorMode } = useColorMode()

  return (
    <IconButton aria-label='Change colour scheme' colorScheme='gray' onClick={toggleColorMode} icon={colorMode === 'light' ? <MoonIcon /> : <SunIcon />} />
  )
}
