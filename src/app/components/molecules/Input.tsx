import {
  Input as ChakraInput,
  InputProps as ChakraInputProps,
} from '@chakra-ui/react'
import { useFormikField } from '../../hooks/useFormikField'

function Input({ name, ...props }: { name: string, props: Omit<ChakraInputProps, 'name'> }) {
  const { field } = useFormikField(name);

  return (
    <ChakraInput {...props} {...field} />
  )
}

export { Input }
