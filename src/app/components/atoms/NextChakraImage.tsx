import { chakra } from '@chakra-ui/react'
import NextImage from 'next/image'

const NextChakraImage = chakra(NextImage, {
  shouldForwardProp: (prop) => ['alt', 'height', 'width', 'src'].includes(prop),
})

export default NextChakraImage