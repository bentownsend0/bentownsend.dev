interface ContactProps {
  href: string
  text: string
}

const recipient: string = 'Codat'
//const mission: string = `${recipient}\'s mission to build money without borders is something I appreciate as a ${recipient} customer myself. I am eager to use my experience in simplifying complex technical issues to develop solutions that align with ${recipient}\'s strategic goals and enhance user experience, making finance easier for everyone.`
const mission: string = `${recipient}\'s mission to deliver useful technology that solves real problems for real businesses is something I appreciate as a problem solver who often turns to technology. I am eager to use my experience in simplifying complex technical issues to develop solutions that align with ${recipient}\'s strategic goals and enhance user experience, making finance easier for everyone.`
const position: string = 'Engineering Manager'
const salary: string = 'I\'m flexible regarding salary and would prefer to discuss it once I have a better understanding of the role and the value I can bring to your team. Additionally, it would be helpful to know the budget range you have allocated for this role.'

export const baseLetter: Array<string> = [
  `Dear ${recipient},`,
  `I\'m excited to apply for the ${position} position. With over 10 years in full-stack development and DevOps, I have worked on many impactful, cross-functional projects. I\'m keen to bring my tech skills, leadership experience, and love for problem-solving to the team.`,
  // `I\'m excited to apply for one of the positions in Engineering. With over 10 years in full-stack development and DevOps, I have worked on many impactful, cross-functional projects. I\'m keen to bring my tech skills, leadership experience, and love for problem-solving to the team.`,
  'I built many business-critical solutions at Immediate Media. One example was developing the infrastructure to better handle incoming traffic for the digital publishing platform. This was based on a project I had previously been involved in, to increase the speed and reduce the number of redirects made. I focused on the AWS well-architected framework and utilised a combination of serverless functions and containerisation. I then took this expertise to New Scientist, where I independently redesigned the infrastructure as the sole DevOps Engineer.',
  'I\'ve gained managerial experience by conducting interviews, focusing on candidates with the necessary skills and also fitted the company culture. Additionally, I represented the company at industry meetups, collaborating with partners to enhance my learning and working towards product improvements through feedback. I\'ve mentored junior developers to channel their creativity into structured business cases, easing their frustrations with the processes and improving project outcomes. I also supported an introverted colleague transitioning from development to infrastructure, encouraging her to find solutions independently while offering support through code reviews and ensuring she had a voice in meetings. I believe facilitating an environment of open conversation fosters a better working culture and improves business solutions.',
  `${mission}`,
  'I\'m eager to enhance my non-technical skills and those of my team. While I can write code, complete tasks, and write tickets, I believe real change comes from empowering people to showcase their strengths, making them more effective team members. I aim to facilitate effective problem-solving at the initial stages of a project to avoid circular discussion. This would allow engineers to get on with what they do best and streamline processes.',
  `I\'d love to discuss how my background and passion can contribute to ${recipient}\'s success. Thank you for considering my application.`,
  'Kind regards,',
  'Ben Townsend'
]

export const contacts: Array<ContactProps> = [{
  href: `tel: ${process.env.NEXT_PUBLIC_CONTACT_NUMBER}`,
  text: process.env.NEXT_PUBLIC_CONTACT_NUMBER || 'Phone'
}, {
  href: `mailto: ${process.env.NEXT_PUBLIC_CONTACT_EMAIL}`,
  text: process.env.NEXT_PUBLIC_CONTACT_EMAIL || 'EMAIL'
}, {
  href: `https://${process.env.NEXT_PUBLIC_CONTACT_LINKEDIN}`,
  text: process.env.NEXT_PUBLIC_CONTACT_LINKEDIN || 'Linkedin'
}, {
  href: `https://${process.env.NEXT_PUBLIC_CONTACT_WEBSITE}`,
  text: process.env.NEXT_PUBLIC_CONTACT_WEBSITE || 'Website'
}]
