'use client'

import { useEffect, useState } from 'react'
import { Box, Container, Heading, IconButton, Stack, Text, useColorModeValue } from '@chakra-ui/react'
import { Link } from '@chakra-ui/next-js'
import { FaPrint as PrintIcon, FaDownload as DownloadIcon } from 'react-icons/fa'

import { baseLetter, contacts } from './pageData'
import { CoverLetterForm } from '../components/organisms/CoverLetterForm'

export default function Page() {
  // const [recipient, setRecipient] = useState('Monzo')
  // const [mission, setMission] = useState(`${recipient}\'s mission to create seamless and impactful giving experiences really resonates with me as a regular giver to multiple organisations. I love the idea of using my skills to help people make a difference in their daily lives. I am eager to use my experience in simplifying complex technical issues and develop solutions that align with ${recipient}\'s strategic goals and enhance user engagement, making giving more accessible and meaningful for everyone.`)
  // const [completeLetter, setCompletedLetter] = useState([''])
  const bgColour = useColorModeValue('white', 'gray.700')
  const titlePink = useColorModeValue('pink.700', 'pink.300')

  // useEffect(() => {
  //   setCompletedLetter(() => {
  //     return ([
  //       `Dear ${recipient},`
  //     ]
  //       .concat(baseLetter)
  //       .splice(3, 0, mission))

  //     console.log(completeLetter)
  //   }
  //   )

  // }, [recipient, mission, baseLetter])

  // const formHandler = () => {

  // }

  //if (recipient && mission) {
  return (
    <Box backgroundColor={bgColour} py='10' sx={{ '@media print': { backgroundColor: 'white', color: 'gray.900', padding: 0 } }}>
      <Container maxWidth='4xl' px='8' pb='10' pos='relative'>
        <Heading as='h1' color='blue.900' size='xl' sx={{ '@media not print': { display: 'none' } }}>Ben Townsend</Heading>
        <Stack color={titlePink} direction={{ base: 'column', md: 'row' }} pt='10' spacing={{ base: '3', md: '10' }} sx={{ '@media print': { color: 'pink.700', flexDirection: 'row', pt: '0' } }}>
          {contacts.map((contact, index) => <Link href={contact.href} key={index} minWidth='max-content'><Heading as='h2' fontWeight={400} size='md'>{contact.text}</Heading></Link>)}
        </Stack>
        <Box pb='1'>
          {baseLetter?.map((paragraph, index) => <Text key={index} pt={4}>{paragraph}</Text>)}
        </Box>
        <Stack bottom={{ base: '8', md: 'auto' }} direction='row' gap='1' pos='fixed' right='8' sx={{ '@media print': { display: 'none' } }} top={{ base: 'auto', md: '160' }}>
          <Link href='/BenTownsendCoverLetter.pdf' download='BenTownsendCoverLetter.pdf' isExternal>
            <IconButton
              aria-label='Download Cover Letter'
              colorScheme='blue'
              icon={<DownloadIcon />}
              size='md'
            />
          </Link>
          <IconButton
            aria-label='Print Cover Letter'
            colorScheme='blue'
            icon={<PrintIcon />}
            onClick={() => window.print()}
            size='md'
          />
        </Stack>
      </Container >
    </Box >
  )
  // } else {
  //   return (
  //     <Box>
  //       <CoverLetterForm></CoverLetterForm>
  //     </Box>
  //   )
  // }
}