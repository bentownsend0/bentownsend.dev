'use client'

import { Box, Button, Container, Heading, ListItem, Stack, Text, UnorderedList, useColorModeValue } from '@chakra-ui/react'
import { ExternalLinkIcon } from '@chakra-ui/icons'
// import { FaExternalLinkAlt as ExternalLinkIcon } from 'react-icons/fa'
import { Link } from '@chakra-ui/next-js'
import Image from './components/atoms/NextChakraImage'
import profile from '../../public/profile.jpeg'

export default function Page() {
  const bgColour = useColorModeValue('white', 'gray.700')

  return (
    <Box backgroundColor={bgColour} py='8'>
      <Container maxWidth='4xl' px='8' py='5'>
        <Heading as='h2' size='3xl' textAlign={{ base: 'center', md: 'left' }} pb='8'>Excuse the lack of content</Heading>
        <Stack alignItems='center' direction={{ base: 'column', md: 'row' }} gap='8' mb='10'>
          <Image alt='Ben Townsend (planting rice in Laos)' src={profile} borderRadius='full' height='100%' maxH='150px' maxW='150px' objectFit='cover' width='100%' />
          <Box textAlign={{ base: 'center', md: 'left' }}>
            <Text as='p' pb='3'>I&apos;m a busy Developer/Engineer/Architect that hasn&apos;t taken the time to build a full portfolio site. I&apos;ve prioritised the work I do for others over building a brand, so for details on skills &amp; experience...</Text>
            <Button as={Link} aria-label='Linkedin profile link' colorScheme='blue' href={process.env.NEXT_PUBLIC_CONTACT_LINKEDIN} isExternal rounded='md'>see Linkedin. <ExternalLinkIcon ml='1' /></Button>
          </Box>
        </Stack>
        <Heading as='h3' size='lg' pb='3'>Notable brands I&apos;ve worked with:</Heading>
        <UnorderedList pb='10'>
          <ListItem key='work1'>Great Western Railway - Communication of rebrand from First Great Western.</ListItem>
          <ListItem key='work2'>Vita Coco - 7 Days 7 Ways event microsite &amp; kids coconut shy campaign game.</ListItem>
          <ListItem key='work3'>BBC Good Food - Infrastructure for 2020 site rebuild.</ListItem>
          <ListItem key='work4'>RadioTimes - Full-stack development &amp; infrastructure.</ListItem>
          <ListItem key='work5'>New Scientist - Infrastructure</ListItem>
          <ListItem key='work6'>Gardeners World - Full-stack development &amp; infrastructure.</ListItem>
          <ListItem key='work7'>Olive Magazine - Full-stack development &amp; infrastructure.</ListItem>
          <ListItem key='work8'>Made For Mums - Infrastructure.</ListItem>
          <ListItem key='work9'>Hitched - Infrastructure.</ListItem>
          <ListItem key='work10'>You and Your Wedding - Infrastructure.</ListItem>
        </UnorderedList>
      </Container>
    </Box >
  )
}