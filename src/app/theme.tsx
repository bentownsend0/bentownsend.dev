import { extendTheme, type ThemeConfig } from '@chakra-ui/react';
import { mode } from '@chakra-ui/theme-tools'
import type { StyleFunctionProps } from '@chakra-ui/styled-system'

const config: ThemeConfig = {
  initialColorMode: 'system',
  useSystemColorMode: false,
}

const activeLabelStyles = {
  transform: 'scale(0.85) translateY(-24px)'
};

export const theme = extendTheme({
  components: {
    Form: {
      variants: {
        floating: {
          container: {
            _focusWithin: {
              label: {
                ...activeLabelStyles
              }
            },
            'input:not(:placeholder-shown) + label, .chakra-select__wrapper + label, textarea:not(:placeholder-shown) ~ label': {
              ...activeLabelStyles
            },
            label: {
              top: 0,
              left: 0,
              zIndex: 2,
              position: 'absolute',
              mx: 3,
              my: 2,
              px: 1,
              bgColor: 'var(--modal-bg)',
              pointerEvents: 'none',
              transformOrigin: 'left top'
            }
          }
        }
      }
    },
    Heading: {
      baseStyle: {
        letterSpacing: '.03em'
      }
    }
  },
  config,
  fonts: {
    heading: 'var(--font-brand-heading)',
    body: 'var(--font-brand-text)',
  },
  styles: {
    global: (props: StyleFunctionProps) => ({
      html: {
        minHeight: '100vh',
        fontSize: '100%'
      },
      body: {
        position: 'relative',
        minHeight: '100vh',
        fontSize: '100%',
        bg: mode('gray.100', 'gray.900')(props),
        color: mode('gray.900', 'white')(props)
      }
    })
  }
});
