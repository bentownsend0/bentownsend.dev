#robots.txt

# Block all crawlers for /coverletter
User-agent: *
Disallow: /coverletter

# Allow all crawlers
User-agent: *
Allow: /