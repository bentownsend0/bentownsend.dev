import { useField } from 'formik'

const useFormikField = (name: string) => {
  const [field] = useField(name)
  return { field }
}

export { useFormikField }