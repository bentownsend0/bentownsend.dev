'use client'

import { ChakraProvider, localStorageManager } from '@chakra-ui/react'
import { theme } from './theme'

export function Providers({ children }: { children: React.ReactNode }) {
  return <ChakraProvider colorModeManager={localStorageManager} theme={theme}>{children}</ChakraProvider>
}
