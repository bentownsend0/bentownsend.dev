import { Bebas_Neue, Roboto } from 'next/font/google'

const heading = Bebas_Neue({
  subsets: ['latin'],
  variable: '--font-brand-heading',
  weight: '400'
})

const text = Roboto({
  subsets: ['latin'],
  variable: '--font-brand-text',
  weight: '400'
})

export const fonts = {
  heading,
  text
}