const fs = require('fs')
const puppeteer = require('puppeteer')

  ; (async () => {
    const HTMLcontent = fs.readFileSync('.next/server/app/cv/page.js', 'utf8')

    const browser = await puppeteer.launch({
      headless: true,
      args: [
        '--no-sandbox',
        '--disable-setuid-sandbox',
        '--font-render-hinting=none',
      ],
    })

    const page = await browser.newPage()
    await page.setContent('')
    await page.addScriptTag(HTMLcontent, {
      waitUntil: ['networkidle0'],
    })
    await page.evaluateHandle('document.fonts.ready')

    await page.pdf({
      path: '../BenTownsendCV.pdf',
      format: 'A4',
      scale: 0.67,
      margin: {
        top: '10mm',
        left: '10mm',
        right: '10mm',
        bottom: '10mm',
      },
    })
    await browser.close()
  })()
